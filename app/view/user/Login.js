Ext.define('EgiRahmadiPutra.view.user.Login', {
    extend: 'Ext.form.Panel',

    requires: [
        'Ext.form.FieldSet',
        'Ext.field.Password'

    ],
    shadow: true,
    xtype: 'login',
    controller : 'login',
    id: 'loginform',
    items: [
        {
            xtype: 'fieldset',
            id: 'fieldsetlogin',
            title: 'Login Form',
            instructions: 'Masukkan Username dan Password dengan Benar',
            defaults: {
                labelWidth: '35%'
            },
            items: [
                {
                    xtype: 'textfield',
                    name: 'username',
                    label: 'Username',
                    required: true,
                    clearIcon: true
                },
                {
                    xtype: 'passwordfield',
                    name: 'password',
                    label: 'Password',
                    clearIcon: true
                }
            ]
        },
        {
            xtype: 'container',
            defaults: {
            xtype: 'button',
            style: 'margin: 1em',
            flex: 1
            },

            layout: {
                    type: 'hbox'
                },
            
                items: [
                {
                    text: 'Login',
                    ui: 'action',
                    //scope: this,
                    hasDisabled: false,
                    handler: 'onLogin'
                },
                {
                    text: 'Reset',
                    ui: 'action',
                    handler: function(){
                        Ext.getCmp('loginform').reset();
                    }
                },
                {
                    text: 'Register',
                    ui: 'action',
                    hasDisabled: false,
                    handler: function(){
                    if (!this.overlay) {
                    this.overlay = Ext.Viewport.add({
                        xtype: 'panel',
                        items:[{
                            docked:'bottom',
                            xtype: 'button',
                            ui : 'action',
                            text: 'Register',
                            handler: function(){
                                this.overlay.hide();
                            }
                        }],
                        floated: true,
                        modal: true,
                        hideOnMaskTap: true,
                        showAnimation:{
                            type: 'popIn',
                            duration: '250',
                            easting: 'east-out'
                        },
                        hideAnimation:{
                            type: 'popOut',
                            duration: '250',
                            easting: 'east-out'
                        },
                        centered: true,
                        width: '100%',
                        height: '100%',
                        styleHtmlContent: true,
                        header: {
                            title: 'Register'
                        },
                        scrollable: true
                    });
                }

                this.overlay.show();

                    }
                }
            ]
        }
    ]
});

