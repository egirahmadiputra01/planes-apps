Ext.define('Pertemuan.view.user.User', {
    extend: 'Ext.form.Panel',

    requires: [
        'Ext.form.FieldSet',
        'Ext.field.Number',
        'Ext.field.Spinner',
        'Ext.field.Password',
        'Ext.field.Email',
        'Ext.field.Url',
        'Ext.field.DatePicker',
        'Ext.field.Select',
        'Ext.field.Hidden',
        'Ext.field.Radio'
    ],
    shadow: true,
    xtype: 'user1',
    type : 'hbox',
    id: 'user1',
    items: [
        {
            xtype: 'fieldset',
            id: 'fieldset1',
            title: 'Personal Info',
            instructions: 'Masukkan Biodata Anda dengan Benar.',
            defaults: {
                labelWidth: '35%'
            },
            items: [
                {
                    xtype: 'textfield',
                    name: 'name',
                    label: 'Name',
                    placeHolder: 'Nama Lengkap',
                    autoCapitalize: true,
                    required: true,
                    clearIcon: true
                },
                {
                    xtype: 'emailfield',
                    name: 'email',
                    label: 'Email',
                    placeHolder: 'Masukkan Email',
                    clearIcon: true
                },
                {
                    xtype: 'urlfield',
                    name: 'url',
                    label: 'No Hp',
                    placeHolder: 'Masukkan No Hp',
                    clearIcon: true
                },
                {
                    xtype: 'datepickerfield',
                    destroyPickerOnHide: true,
                    name: 'date',
                    label: 'Tanggal Lahir',
                    value: new Date(),
                    picker: {
                        yearFrom: 1990
                    }
                },
                {
                    xtype: 'selectfield',
                    name: 'rank',
                    label: 'Status',
                    options: [
                        {
                            text: 'Pelajar/Mahasiswa',
                            value: 'pelajar'
                        },
                        {
                            text: 'PNS',
                            value: 'pns'
                        },
                        {
                            text: 'Lainya',
                            value: 'lainya'
                        }
                    ]
                },
                {
                    xtype: 'passwordfield',
                    revealable: true,
                    name : 'password',
                    label: 'Password',
                    placeHolder: 'Masukkan Password',
                    clearIcon: true
                },
    
            ]
        },
        {
            xtype: 'container',
            defaults: {
                xtype: 'button',
                style: 'margin: 1em',
                flex: 1
            },
            layout: {
                type: 'hbox'
            },
            items: [
                {
                    text: 'Save',
                    ui: 'action',
                    scope: this,
                    hasDisabled: false,
                    handler: function(btn){
                        Ext.Msg.alert('Data Tersimpan','Data Udah Tersimpat bro',Ext.emptyFn);
                    }
                },
                {
                    text: 'Reset',
                    ui: 'action',
                    handler: function(){
                        Ext.getCmp('basicform').reset();
                    }
                }
                
            ]
        }
    ]
});

Ext.define('EgiRahmadiPutra.view.main.user.User', {
    extend: 'Ext.tab.Panel',

    shadow: true,
    xtype: 'user',
    type : 'vbox',
    id: 'user',
    tabBar: {
        layout: {
            pack: 'center'
        }
    },
    activeTab: 1,
    defaults: {
        scrollable: true
    },
    items: [
        {
            title: 'Daftar Account',
            items: [{
                xtype: 'user1'
            }]
        },
        {
            title: 'Login',
            html : 'Halaman Login Belum Tersedia',
           
        }
    ]
});