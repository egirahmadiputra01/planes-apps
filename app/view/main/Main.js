/**
 * This class is the main view for the application. It is specified in app.js as the
 * "mainView" property. That setting causes an instance of this class to be created and
 * added to the Viewport container.
 *
 * TODO - Replace the content of this view to suit the needs of your application.
 */
Ext.define('EgiRahmadiPutra.view.main.Main', {
    extend: 'Ext.tab.Panel',
    xtype: 'app-main',

    requires: [
        'Ext.MessageBox',

        'EgiRahmadiPutra.view.main.MainController',
        'EgiRahmadiPutra.view.main.MainModel',
        'EgiRahmadiPutra.view.main.BasicDataView',
        'EgiRahmadiPutra.view.main.Video',
        'EgiRahmadiPutra.view.user.Login',
        'EgiRahmadiPutra.view.main.Audio',
        'EgiRahmadiPutra.view.main.Video',
    ],

    controller: 'main',
    viewModel: 'main',

    defaults: {
        tab: {
            iconAlign: 'top'
        },
        styleHtmlContent: true
    },

    tabBarPosition: 'bottom',

    items: [
        {
            title: 'Maskapai',
            iconCls: 'x-fa fa-plane',
            layout: 'fit',
            // The following grid shares a store with the classic version's grid as well!
            items: [{
                xtype: 'basicdataview'
            }]
        },{
            title: 'Video',
            iconCls: 'x-fa fa-video-camera',
            layout: 'fit',
            items: [{
                xtype: 'Movie'
            }]
        },{
            title: 'Audio',
            iconCls: 'x-fa fa-info-circle',
            layout: 'fit',
            items: [{
                xtype: 'music'
            }]
        },{
            title: 'Account',
            iconCls: 'x-fa fa-user',
            layout: 'fit',
            items: [{
                xtype: 'user'
            }]
        }
    ]
});
