Ext.define('EgiRahmadiPutra.view.main.Video', {
    extend: 'Ext.Container',
    requires: [
        'Ext.Video'
    ],
    xtype: 'Movie',
    layout: 'fit',
    shadow: true,
    listeners: {
        hide: function () {
            try {
                var video = this.down('video');
                video.fireEvent('hide');
            }
            catch (e) {
            }
        },
        show: function () {
            try {
                var video = this.down('video');
                video.fireEvent('show');
            }
            catch (e) {
            }
        }
    },
    items: [{
        xtype: 'video',
        url: 'resources/video/videoplayback.mp4',
        loop: true,
        posterUrl: 'resources/video/Cover.jpg'
    }]
});