/**
 * This class is the controller for the main view for the application. It is specified as
 * the "controller" of the Main view class.
 *
 * TODO - Replace this content of this view to suite the needs of your application.
 */
Ext.define('EgiRahmadiPutra.view.main.MainController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.main',

    onItemSelected: function (sender, record) {
        Ext.Msg.confirm('Confirm', 'Are you sure?', 'onConfirm', this);
    },

    onConfirm: function (choice) {
        if (choice === 'yes') {
            //
        }
    },

    onLogout: function (choice) {
        Ext.Msg.confirm('Logout','Anda Benar-Benar Yakin?','onConfirm',this);
    },

    onDownload: function (choice) {
        Ext.Msg.confirm('Download','Data Tersimpan (Sama-sama)',this);
    },
    onInfo: function (choice) {
        Ext.Msg.confirm('Info','Ini Info<br>Udah, Itu aja',this);
    }
});
