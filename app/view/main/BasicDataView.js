/**
 * This view is an example list of people.
 */


Ext.define('EgiRahmadiPutra.view.main.BasicDataView', {
    extend: 'Ext.Container',
    xtype: 'basicdataview',

    requires: [
        'Ext.dataview.plugin.ItemTip',
        'Ext.plugin.Responsive',
        'EgiRahmadiPutra.store.Personnel',
        'Ext.field.Search'
    ],

    viewModel: {
        stores: {
            personnel: {
                type: 'personnel'
            }
        }
    },

    layout: 'fit',
    cls: 'ks-basic demo-solid-background',
    shadow: true,
    items: [
    {
        docked: 'top',
        xtype: 'toolbar',
        items: [
        {
            xtype: 'searchfield',
            placeHolder: 'Cari Maskapai',
            name: 'searchfield',
            listeners: {
                change: function ( me, newValue, oldValue, eOpts){
                    personnelStore = Ext.getStore('personnel');
                    personnelStore.filter('maskapai', newValue);
                }
            }
        }]
    },
    {
        xtype: 'toolbar',
        docked: 'top',
        items: [
        {
            xtype: 'spacer'
        }, 
        {
            text: 'Download',
            iconCls: 'x-fa fa-download',
            handler: 'onDownload'
        }, 
        {
            text: 'INFO',
            iconCls: 'x-fa fa-info-circle',
            handler: 'onInfo'
        },
        {
            text: 'Logout',
            iconCls: 'x-fa fa-refresh',
            handler: 'onLogout'
        }]
    },
    {
        xtype: 'dataview',
        scrollable: 'y',
        cls: 'dataview-basic',
        itemTpl:'<div class="card" style="width:450px">'+ 
                '<img class="card-img-top" style="width:30% height:auto">{photo}'+
                '<div class="card-body">'+
                '<h2 class="card-title text-primary"><strong>Maskapai &emsp; : {maskapai}</strong></h2>'+
                '<p class="card-text">Website &emsp;&emsp;: {website}</p>'+
                '<p class="card-text">Tahun Awal &nbsp; : {tahun}</p>'+
                '<p class="card-text">Negara &emsp; &emsp; : {negara}</p>'+
                '<p class="card-text">Status &emsp; &emsp; &nbsp;: {status}</p><br>'+
                '<button class="btn btn-primary">Detail</button>'+
                '</div>'+
                '</div><br>',

    listeners: {
        element: 'element',
        delegate: '.btn',
        Click: function ( out, value, parent, xindex, xcount, xkey ){
             Ext.Msg.alert('Detail', 'Ini adalah detail Maskapai Pilihan Anda', Ext.emptyFn);            
        }, 
    },

        
        bind: {
            store: '{personnel}',
        },
        
        plugins: {
            type: 'dataviewtip',
            align: 'l-r?',
            plugins: 'responsive',
            
            // On small form factor, display below.
            responsiveConfig: {
                "width < 600": {
                    align: 'tl-bl?'
                }
            },
            width: 200,
            minWidth: 300,
            delegate: '.img',
            allowOver: true,
            anchor: true,
            bind: '{record}',
            tpl: '<table style="border-spacing:3px;border-collapse:separate">' + 
                    '<tr><td>Maskapai: </td><td>{maskapai}</td></tr>' +
                    '<tr><td>Negara:</td><td>{negara}</td></tr>' + 
                    '<tr><td>Tahun:</td><td>{tahun}</td></tr>' +
                    '<tr><td>Website:</td><td>{website}</td></tr>'+
                    '<tr><td vAlign="top">Ulasan:</td><td><div style="max-height:100px;overflow:auto;padding:1px">{tentang}</div></td></tr>' 
                   
        }
    }]
});

